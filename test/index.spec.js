'use strict'

const assert = require('assert')
const getMaxSubstring = require('../lib')

describe('max correct bracket sequence', () => {

    const tests = [
        {
            description: 'test 1',
            params: ')))(((',
            expected: 'Infinite',
        },
        {
            description: 'test 2',
            params: ')))))) (',
            expected: '()'
        },
        {
            description: 'test 3',
            params: ')(())))()))) (',
            expected: '()(())'
        },
        {
            description: 'test 4',
            params: '[(])',
            expected: ''
        },
        {
            description: 'test 5',
            params: '(}',
            expected: ''
        },
        {
            description: 'test 6',
            params: '({[]}}',
            expected: '{[]}'
        },
        {
            description: 'test 66',
            params: 'a(s{d[f]g}h}j',//a(s{d[f]g}h}ja(s{d[f]g}h}j
            expected: 's{d[f]g}h'
        },
        {
            description: 'test 666',
            params: '([{[]}}]))',
            expected: '{[]}'
        },
        {
            description: 'test 6666',
            params: '1(2[3{4[555]6}77}8]9)10)11',
            expected: ''
        },
        {
            description: 'test 7',
            params: 'a)s)df)(kl(;;(lk',
            expected: '(lka)s',
        },
        {
            description: 'test 8',
            params: 'q)w)er)ty)ui)op)pop(iuy',
            expected: 'pop(iuyq)w'
        },
        {
            description: 'test 88',
            params: 'q)1w)er)ty)ui)op)99p(iuy',
            expected: 'p(iuyq)'
        },
        {
            description: 'test 9',
            params: 'a(b)a}a',
            expected: 'aa(b)a',
        },
        {
            description: 'test 10',
            params: 'a',
            expected: 'Infinite',
        },
        {
            description: 'test 101',
            params: 'aaa',
            expected: 'Infinite',
        },
        {
            description: 'test 11',
            params: '',
            expected: '',
        },
        {
            description: 'test 12',
            params: '((())',
            expected: '(())',
        },
        {
            description: 'test 13',
            params: 'q(w(e(r)t)y',
            expected: 'w(e(r)t)y',
        },
        {
            description: 'test 14',
            params: '([{[]}}]))',
            expected: '{[]}'
        },
        {
            description: 'test 15',
            params: '343!!',
            expected: '',
        },
    ]

    tests.forEach(({ description, params, expected }) => {

        it(`${description}: "${params}" -> "${expected}"`, () => {

            const actual = getMaxSubstring(params)
            assert.deepStrictEqual(actual, expected)
        })
    })
})
