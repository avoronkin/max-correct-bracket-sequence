'use strict'

const brackets = {
    '(': ')',
    '[': ']',
    '{': '}'
}
const openBrackets = Object.keys(brackets)
const closeBrackets = Object.values(brackets)

module.exports = function getMaxSubstring (str) {
    return _getMaxSubstring(str + str, str.length)
}

function _getMaxSubstring (str, maxLength) {
    let tempStr = ''
    let max = ''
    let stack = []

    for (let i = 0; i < str.length; i++) {
        if (tempStr.length === maxLength) {
            if (stack.length) {
                max = reverseBrackets(_getMaxSubstring(reverseBrackets(tempStr), tempStr.length))
            } else {
                max = 'Infinite'
            }

            break
        }

        const char = str[i]
        const isCloseBracket = closeBrackets.includes(char)
        const isOpenBracket = openBrackets.includes(char)
        const isLatin = isLatinAlphabet(char)
        const lastBracket = stack[stack.length - 1]
        const isValidCloseBracket = stack.length && isCloseBracket && brackets[lastBracket] === char
        const notValid = (!isValidCloseBracket && !isLatin) || (!stack.length && isCloseBracket)

        if (isOpenBracket || isLatin || isValidCloseBracket) {
            tempStr += char
        }

        if (isOpenBracket) {
            stack.push(char)
            continue
        }

        if (isValidCloseBracket) {
            stack.pop()
            continue
        }

        if (notValid) {

            if (stack.length) {
                tempStr = reverseBrackets(_getMaxSubstring(reverseBrackets(tempStr), tempStr.length))
                stack = []
            }

            if (tempStr.length > max.length) {
                max = tempStr
            }

            tempStr = ''
        }
    }

    return max
}

const reversedBrackets = {
    '(': ')',
    ')': '(',
    '[': ']',
    ']': '[',
    '{': '}',
    '}': '{',
}

function reverseBrackets (str) {
    return str
        .split('')
        .reverse()
        .map(s => reversedBrackets[s] || s)
        .join('')
}

const latinRe = /^[a-zA-Z]+$/

function isLatinAlphabet (str) {
    return latinRe.test(str)
}
